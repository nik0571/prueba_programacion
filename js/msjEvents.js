/* ==========================================================================
   Functions - msj events
   ========================================================================== */
/**
*	Funcion para generar un identificador aleatorio
*	para cada suceso
*/
function generateIdEvent() {
    var id = 'event-';
    var x = Math.floor((Math.random() * 500) + 1);
    id +=x;
    return id;
}

/**
*	Funcion para remover los mensajes de suceso
*  	var id type[string] = identificador del mensaje a remover
*/
function removeMsjEvent(id){
  $('#'+id).remove();
}

/**
 * [append_msj Funcion para agregar mensajes de suceso
 * @param  {string} str [mensaje a imprimir]
 */
function append_msj(str){

  if(str == 'default'){
    str = 'Se ha producido un error, por favor intente nuevamente...'
  }

  var idEvent = generateIdEvent();
    $('#sucesos').append('<div id="'+idEvent+'" class="msj_suceso"><aside>'+str+'</aside>'+'<a class="event_close" event-close="'+idEvent+'">X</a></div>');
    setTimeout(function(){
        removeMsjEvent(idEvent);
    },6000);
}

/**
*	Con esto detecto el click para cerrar el mensaje
*/
$('#sucesos').on('click', '[event-close]' ,function() {
  removeMsjEvent($(this).attr('event-close'));
});