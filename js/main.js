/**
 * Main js
 */

//cargar el banner activo
var cargar_banner = function(){
	$.ajax({
	    url : 'controller/cargarBannerController.php',
	    data : {accion: 'Cargar'},
	    type: 'POST',
	  	dataType: 'json'
	  })
	  .done(function(result) {
	  	(result.suceso) ? $('#container-banner').html(result.msj) : append_msj(result.msj);
	  })
	  .fail(function(jqXHR) {
	  	append_msj('default');
	    console.log(jqXHR.statusText);
	    console.log(jqXHR.responseText);
	  })
	  .always(function() {
	  	console.log("complete");
	});
}();


/**
 * Funcion para imprimir los productos en el DOM
 * @param  {[type]} datos [description]
 * @return {[type]}       [description]
 */
var print_procuctos = function (datos){
	var html = '<h2>Productos</h2>';
	$.each(datos, function(index, el) {
		html += '<div class="row">';
			html += '<h2> '+el.nombre_pro+' </h2>';
			html += '<img src="img/productos/'+el.img_pro+'" alt="Producto">';
			html += '<p>'+el.descrip_pro+'</p>';
		html += '</div>';
	});

	$('#productos').html(html);
}

//cargar productos
var cargar_productos = function(){
	$.ajax({
	    url : 'controller/cargarProductosController.php',
	    data : {accion: 'Cargar'},
	    type: 'POST',
	  	dataType: 'json'
	  })
	  .done(function(result) {
	  	(result.suceso) ? print_procuctos(result.datos) : append_msj(result.msj);
	  })
	  .fail(function(jqXHR) {
	  	append_msj('default');
	    console.log(jqXHR.statusText);
	    console.log(jqXHR.responseText);
	  })
	  .always(function() {
	  	console.log("complete");
	});
}();

//Funcion para desplazarse en las distintas secciones de la web
  $(function(){
    //clic en un enlace de la lista
    $('[data-seccion]').on('click',function(e){

        //prevenir en comportamiento predeterminado del enlace
        e.preventDefault();

        //obtenemos el id del elemento en el que debemos posicionarnos
        var strAncla= $(this).attr("data-seccion");
         
        //utilizamos body y html, ya que dependiendo del navegador uno u otro no funciona
        $('body,html').stop(true,true).animate({
            //realizamos la animacion hacia el ancla
            scrollTop: $(strAncla).offset().top
        },1000);
    });
});


/**
 * send_msj : Funcion para comunicarse con el controlador
 * @param  datos [varibles o datos a se enviados]
 * @param  url   [destino en el cual se encuentra el controlador]
 */
var send_msj = function(datos,url){

	$.ajax({
	    url : url,
	    data : datos,
	    type: 'POST',
	  	dataType: 'json'
	  })
	  .done(function(result) {
	  	if(result.suceso){
	  		$('#enviarMensaje input').val('');
	  		$('#enviarMensaje textarea').val('');
	  	}
	  	append_msj(result.msj); // le damos respuesta al usuario
	  })
	  .fail(function(jqXHR) {
	  	append_msj('default');
	    console.log(jqXHR.statusText);
	    console.log(jqXHR.responseText);
	  })
	  .always(function() {
	  	console.log("complete");
	  });
}


//Se valida las credenciales de acceso
//y se hace login en caso de que se apruben las credenciales
var login = function(datos,url){
	$.ajax({
	    url : url,
	    data : datos,
	    type: 'POST',
	  	dataType: 'json',
	  	cache: false
	  })
	  .done(function(result) {
	  	(result.suceso)? location.href='admins/view/cpanel.php' : append_msj(result.msj);
	  })
	  .fail(function(jqXHR) {
	  	append_msj('default');
	    console.log(jqXHR.statusText);
	    console.log(jqXHR.responseText);
	  })
	  .always(function() {
	  	console.log("complete");
	  });
}


$(window).load(function (){
	/**
	 * Se valida el formulario de contacto
	 */
	$('#enviar').click(function (){
		if($('#enviarMensaje').parsley().validate()){
	      	var datos = $('#enviarMensaje').serialize()+"&accion=Enviar";
	      	var action = $('#enviarMensaje').attr('action');
	      	send_msj(datos,action);//se llama la funcion que envia los datos
	        return false;
	    }else{
	        return false;
	    }
	});


	/**
	 * Se Valida el formulario de login
	*/
	$('#login').click(function (){
		if($('#loginForm').parsley().validate()){
	      	var datos = $('#loginForm').serialize()+"&accion=Login";
	      	var action = $('#loginForm').attr('action');
	      	login(datos,action);//se llama la funcion que envia los datos
	        return false;
	    }else{
	        return false;
	    }
	});


});

