<?php

/**
 * Dao publico
 * @author Naycool Gonzalez Cantillo <naycoolgonzalez@gmail.com>
 *         Developer Web
 */

require_once BASEPATH . 'lib/DbPdo.php';


class publicDao {

    protected function _getDbh() {
        return DbPdo::getInstance()->getConn();
    }

    /**
     * [$result variable para enviar los resultados
     * de un query
     * @var array
     */
    public $result = array();

    /**
     * [row_fetch_obj Funcion para ejecutar
     *  un query usando PDO y pasar el resultado
     *  por en FETCH_OBJ]
     * @param  [type] $stm [consulta a ejecutar]
     * @return [Array]      [Resultado del query]
     */
    protected static function row_fetch_obj($stm){

        if($stm->execute()):
            $rows = $stm->fetch(PDO::FETCH_OBJ);
            return $rows;
        else:
            return false;
        endif;
    }

    /**
     * [rows_fetch_obj Funcion para ejecutar
     *  un query usando PDO y pasar el resultado
     *  por en FETCH_OBJ]
     * @param  [type] $stm [consulta a ejecutar]
     * @return [ArrayObject]      [Resultado del query]
     */
    protected static function rows_fetch_obj($stm){

        $obj = new ArrayObject();
        if($stm->execute()):
            while ($rows = $stm->fetch(PDO::FETCH_OBJ))
            {
                $obj->append($rows);
            }
            return $obj;
        else:
            return 'FALSE';
        endif;
    }

    /**
     * [login Funcion para verificar las credenciales del usuario
     * @param  [type] $username [Username]
     * @param  [type] $password [Password]
     */
    public function login($username, $password){

        $sql = "SELECT *
                FROM `users` u
                WHERE u.`username` = ?
                AND u.`password` = ?";

        $stm = $this->_getDbh()->prepare($sql);

        $stm->bindValue(1,  $username, PDO::PARAM_STR);
        $stm->bindValue(2,  md5($password), PDO::PARAM_STR);

        if($stm->execute()):

            if($stm->rowCount()>0){
                $this->result['suceso'] = true;
                $this->result['msj'] = 'Exito al logear';
                $this->result['datos'] = $stm->fetch(PDO::FETCH_OBJ);
                return $this->result;
            }else{
                $this->result['suceso'] = false;
                $this->result['msj'] = 'Error: Credenciales invalidas';
                return $this->result;
            }
        else:
            $this->result['suceso'] = false;
            $this->result['msj'] = 'Error al ejecutar el query';
            return $this->result;
        endif;
    }

    /**
     * [cargar_banner Funcion para cargar el banner activo
     * @return [type] [description]
     */
    public function cargar_banner(){
        $sql = "SELECT *
                FROM `banners` b
                WHERE b.`estado` = 'ACTIVO'
                LIMIT 1";
        $stm = $this->_getDbh()->prepare($sql);

        if($stm->execute()):

            if($stm->rowCount()>0){
                $this->result['suceso'] = true;
                $this->result['msj'] = 'Banner cargado';
                $this->result['banner'] = $stm->fetch(PDO::FETCH_OBJ);
                return $this->result;
            }else{
                $this->result['suceso'] = false;
                $this->result['msj'] = 'Error: Al ejecutar el query';
                return $this->result;
            }
        else:
            $this->result['suceso'] = false;
            $this->result['msj'] = 'Error cargar el banner';
            return $this->result;
        endif;

    }



    /**
     * Funcion para cargar todos los productos
     * @return [type] [description]
     */
    public function load_all_productos(){
        $sql = "SELECT *
                FROM `productos`";

        $stm = $this->_getDbh()->prepare($sql);

        return self::rows_fetch_obj($stm);

    }


}