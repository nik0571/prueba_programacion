<?php

/**
 * Dao AdmimsDao
 * @author Naycool Gonzalez Cantillo <naycoolgonzalez@gmail.com>
 *         Developer Web
 */

require_once BASEPATH . 'lib/DbPdo.php';


class adminsDao {

    protected function _getDbh() {
        return DbPdo::getInstance()->getConn();
    }

    /**
     * [$result variable para enviar los resultados
     * de un query
     * @var array
     */
    public $result = array();

    /**
     * [row_fetch_obj Funcion para ejecutar
     *  un query usando PDO y pasar el resultado
     *  por en FETCH_OBJ]
     * @param  [type] $stm [consulta a ejecutar]
     * @return [Array]      [Resultado del query]
     */
    protected static function row_fetch_obj($stm){

        if($stm->execute()):
            $rows = $stm->fetch(PDO::FETCH_OBJ);
            return $rows;
        else:
            return false;
        endif;
    }

    /**
     * [rows_fetch_obj Funcion para ejecutar
     *  un query usando PDO y pasar el resultado
     *  por en FETCH_OBJ]
     * @param  [type] $stm [consulta a ejecutar]
     * @return [ArrayObject]      [Resultado del query]
     */
    protected static function rows_fetch_obj($stm){

        $obj = new ArrayObject();
        if($stm->execute()):
            while ($rows = $stm->fetch(PDO::FETCH_OBJ))
            {
                $obj->append($rows);
            }
            return $obj;
        else:
            return 'FALSE';
        endif;
    }


    public function insertBanner($name){
        $sql = "INSERT INTO `banners` (`img`) VALUES (?)";

        $stm = $this->_getDbh()->prepare($sql);

        $stm->bindValue(1,  $name, PDO::PARAM_STR);

        if($stm->execute()):
            $id = $this->_getDbh()->lastInsertId();

            return $this->update_estado_banner($id);
        else:
            $id = false;
        endif;
    }

    public function update_estado_banner($id){
        $sql = "UPDATE `banners` SET `estado` = 'INACTIVO'";

        $stm = $this->_getDbh()->prepare($sql);

        if($stm->execute()):

            $sql = "UPDATE `banners` SET `estado` = 'ACTIVO' WHERE `id_banner` = '$id'";

            $stm = $this->_getDbh()->prepare($sql);

            if($stm->execute()):
                return $stm->rowCount();
            else:
                return false;
            endif;

        else:
            return false;
        endif;
    }


    /**
     * [cargar_banner Funcion para cargar todos los banners
     * @return [type] [description]
     */
    public function cargar_all_banners(){
        $sql = "SELECT *
                FROM `banners`
                ORDER BY `estado`";
        $stm = $this->_getDbh()->prepare($sql);

        return self::rows_fetch_obj($stm);

    }

    /**
     * Funcon para insertar un producto
     * @param  [type] $img  [imagen del producto]
     * @param  [type] $name [Nombre del producto]
     * @param  [type] $des  [descripcion del producto]
     * @return [type]       [description]
     */
    public function insertProducto($img,$name,$des){
        $sql = "INSERT INTO `productos` (`img_pro`,`nombre_pro`, `descrip_pro`) VALUES (?,?,?)";

        $stm = $this->_getDbh()->prepare($sql);

        $stm->bindValue(1,  $img, PDO::PARAM_STR);
        $stm->bindValue(2,  $name, PDO::PARAM_STR);
        $stm->bindValue(3,  $des, PDO::PARAM_STR);

        if($stm->execute()):
            return $stm->rowCount();
        else:
            return false;
        endif;
    }


    /**
     * Funcion para cargar todos los productos
     * @return [type] [description]
     */
    public function listar_all_productos(){
        $sql = "SELECT *
                FROM `productos`";

        $stm = $this->_getDbh()->prepare($sql);

        return self::rows_fetch_obj($stm);

    }

}