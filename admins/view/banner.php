<?php
require_once '../../config.ini.php';
require_once BASEPATH.'lib/Simple_sessions.php';
require_once BASEPATH.'admins/models/admisDao.php';

$_obj = new adminsDao();

$banners = $_obj->cargar_all_banners();

$obj_ses = new Simple_sessions();
if ($obj_ses->check_sess('username')): ?>


<?php include_once BASEPATH.'admins/layout/header.php'; ?>

    <section class="container-admins container-banners">
		<h1>Banner del home</h1>


			<form id="new_banner" enctype="multipart/form-data" action="admins/controller/uploadBannerController.php" data-parsley-validate>

				<input type="file" name="imagen" required>

				<button id="submit" type="button">Agregar</button>

			</form>

			<div id="allbanners">
				<?php foreach ($banners as $key => $value) { ?>
					<img class="<?php echo $value->estado; ?>" src="img/banners/<?php echo $value->img; ?>" alt="Banner" data-banner-id="<?php echo $value->id_banner;?>">
				<?php } ?>
			</div>

    </section>

<?php include_once BASEPATH.'admins/layout/footer.php'; ?>

<?php
else:
    header('location: ../');
endif;
?>