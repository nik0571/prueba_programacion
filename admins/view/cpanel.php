<?php
require_once '../../config.ini.php';
require_once BASEPATH.'lib/Simple_sessions.php';
$obj_ses = new Simple_sessions();
if ($obj_ses->check_sess('username')): ?>


<?php include_once BASEPATH.'admins/layout/header.php'; ?>

    <section>

    </section>

<?php include_once BASEPATH.'admins/layout/footer.php'; ?>

<?php
else:
    header('location: ../');
endif;
?>