<?php
require_once '../../config.ini.php';
require_once BASEPATH.'lib/Simple_sessions.php';
require_once BASEPATH.'admins/models/admisDao.php';

$_obj = new adminsDao();

$productos = $_obj->listar_all_productos();

$obj_ses = new Simple_sessions();
if ($obj_ses->check_sess('username')): ?>


<?php include_once BASEPATH.'admins/layout/header.php'; ?>

    <section class="container-admins container-productos">
		<h1>Productos</h1>


			<form id="new_producto" enctype="multipart/form-data" action="admins/controller/uploadProductoController.php" data-parsley-validate>
				<label for="">Imagen del producto</label>
				<input type="file" name="imagen" required>
				<label for="">Nombre del producto</label>
				<input type="text" name="nombre" required>
				<label for="">Descripcion del producto</label>
				<textarea name="descripcion" cols="30" rows="10"></textarea>

				<button id="submit" type="button">Agregar</button>

			</form>

			<div id="allProductos">
				<?php foreach ($productos as $key => $value) { ?>
					<div class="row">
						<h2><?php echo $value->nombre_pro;?></h2>
						<img src="img/productos/<?php echo $value->img_pro ; ?>" alt="Producto">
						<p>
							<?php echo $value->descrip_pro; ?>
						</p>
					</div>
				<?php } ?>
			</div>

    </section>

<?php include_once BASEPATH.'admins/layout/footer.php'; ?>

<?php
else:
    header('location: ../');
endif;
?>