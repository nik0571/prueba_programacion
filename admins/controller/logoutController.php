<?php
	/**
	 * Controlador para cerrar sesion
	 */
	require_once '../../config.ini.php';
	require_once BASEPATH.'lib/Simple_sessions.php'; //libreria para el control del sesiones


	    $id = $_GET['username'];

        $obj_ses = new Simple_sessions();
        $obj_ses->del_sess($id);
        $obj_ses->destroy_sess();

	    header('location:'.BASEURL.'');

exit;
?>

