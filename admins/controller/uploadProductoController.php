<?php
require_once '../../config.ini.php';
require_once BASEPATH.'admins/models/admisDao.php';

$ruta = BASEPATH .'img/productos/';


	$result = array();
	$result['suceso'] = true;

   if(isset($_FILES['imagen'])){

      $file_name = $_FILES['imagen']['name'];
      $file_size =$_FILES['imagen']['size'];
      $file_tmp =$_FILES['imagen']['tmp_name'];
      $file_type=$_FILES['imagen']['type'];
      $file_ext=strtolower(end(explode('.',$_FILES['imagen']['name'])));

      $name = $_POST['nombre'];
      $descripcion = $_POST['descripcion'];

      $expensions= array("jpeg","jpg","png","JPG","PNG","JPEG");

      if(in_array($file_ext,$expensions)=== false){
        $result['suceso'] = false;
        $result['msj'] = "Extencion no permitadia";
      }

      if($file_size > 2097152){
        $result['suceso'] = false;
        $result['msj'] = "Extencion no permitadia";
      }

      if($result['suceso']){
        move_uploaded_file($file_tmp,$ruta.$file_name);


        $_obj = new adminsDao();

        $insert = $_obj->insertProducto($file_name,$name,$descripcion);

        if($insert != false && $insert == 1){
          $result['suceso'] = true;
          $result['msj'] = "Producto creado!!";
        }else{
          $result['suceso'] = false;
          $result['msj'] = "Error al crear el producto";
        }
      }

   }else{
        $result['suceso'] = false;
        $result['msj'] = "No llego la imagen";
   }


   echo json_encode($result);

	exit;

?>