<?php
require_once '../../config.ini.php';
require_once BASEPATH.'lib/inputfilter.php'; //libretia para prevenir ataques XSS
require_once BASEPATH.'lib/Helpers.php'; //ayudante
require_once BASEPATH.'admins/models/admisDao.php';


	$result = array();
	$result['suceso'] = true;

      if(!Helpers::validDatas(array($_POST['id']))){
          $result['suceso']= false;
          $result['msj'] = 'Error: No llegaron algunos de los datos requeridos';
      }else{

        $_clean  = new InputFilter();
        /**
         * Limpio las variables y prevengo ataque XSS
         */
        $id = $_clean->process($_POST['id']);

        $_obj = new adminsDao();

        $update = $_obj->update_estado_banner($id);

        if($update != false && $update == 1):
          $result['suceso']= true;
          $result['msj'] = 'Banner actualizado';
        else:
          $result['suceso']= false;
          $result['msj'] = 'Error al actualizar el banner!!!';
        endif;
      }


   echo json_encode($result);

	exit;

?>