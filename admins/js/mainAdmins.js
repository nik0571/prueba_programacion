/**
 * js admis
 */

/**
 * [new_banner Funcion para agregar banners
 * @return {[type]} [description]
 */
var new_banner = function(){
  var formElement = document.getElementById("new_banner");
  formData = new FormData(formElement);
    $.ajax({
        url : $('#new_banner').attr('action'),
        data : formData,
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        mimeType: "multipart/form-data",
        dataType: 'json'
    })
    .done(function(result) {
      append_msj(result.msj);
    })
    .fail(function(jqXHR) {
      append_msj('default');
      console.log(jqXHR.statusText);
      console.log(jqXHR.responseText);
    })
    .always(function() {
      console.log("complete");
    });
}

var new_producto = function(){
  var formElement = document.getElementById("new_producto");
  formData = new FormData(formElement);
    $.ajax({
        url : $('#new_producto').attr('action'),
        data : formData,
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,
        mimeType: "multipart/form-data",
        dataType: 'json'
    })
    .done(function(result) {
      append_msj(result.msj);
    })
    .fail(function(jqXHR) {
      append_msj('default');
      console.log(jqXHR.statusText);
      console.log(jqXHR.responseText);
    })
    .always(function() {
      console.log("complete");
    });
}

var update_estado_banner = function(idBanner){

    $.ajax({
        url : 'admins/controller/updateEstadoBannerController.php',
        data : {id: idBanner},
        type: 'POST',
        cache: false,
        dataType: 'json'
    })
    .done(function(result) {
      append_msj(result.msj);
      $('[data-banner-id]').removeClass('ACTIVO');
      if(result.suceso){
        $('[data-banner-id="'+idBanner+'"]').addClass('ACTIVO');
      }
    })
    .fail(function(jqXHR) {
      append_msj('default');
      console.log(jqXHR.statusText);
      console.log(jqXHR.responseText);
    })
    .always(function() {
      console.log("complete");
    });
}

$(window).load(function (){
  /**
   * Se valida el formulario de contacto
   */
  $('#new_banner #submit').click(function (){
    console.log('click');

    if($('#new_banner').parsley().validate()){
          new_banner();
          return false;
      }else{
          return false;
      }
  });

  $('#allbanners img').click(function (){
    var idbanner = $(this).attr('data-banner-id');
    update_estado_banner(idbanner);
  });


  $('#new_producto #submit').click(function (){
    console.log('click');

    if($('#new_producto').parsley().validate()){
          new_producto();
          return false;
      }else{
          return false;
      }
  });

});
