/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.26 : Database - db_pruba
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_pruba` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db_pruba`;

/*Table structure for table `banners` */

DROP TABLE IF EXISTS `banners`;

CREATE TABLE `banners` (
  `id_banner` int(3) NOT NULL AUTO_INCREMENT COMMENT 'Identificador del banner',
  `img` varchar(300) NOT NULL COMMENT 'Imagen',
  `estado` enum('ACTIVO','INACTIVO') NOT NULL DEFAULT 'INACTIVO' COMMENT 'Estado del banner',
  PRIMARY KEY (`id_banner`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `banners` */

insert  into `banners`(`id_banner`,`img`,`estado`) values (2,'cropped.png','INACTIVO'),(3,'geek-wallpapers-stugon.com-12.jpg','INACTIVO'),(4,'geek-wallpapers-stugon.com-12.jpg','INACTIVO'),(5,'geek-wallpapers-stugon.com-12.jpg','INACTIVO'),(6,'geek-wallpapers-stugon.com-12.jpg','ACTIVO'),(7,'geek-wallpapers-stugon.com-12.jpg','INACTIVO'),(8,'geek-wallpapers-stugon.com-12.jpg','INACTIVO'),(9,'geek-wallpapers-stugon.com-12.jpg','INACTIVO'),(10,'geek-wallpapers-stugon.com-12.jpg','INACTIVO'),(11,'1825906.png','INACTIVO');

/*Table structure for table `productos` */

DROP TABLE IF EXISTS `productos`;

CREATE TABLE `productos` (
  `id_pro` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `img_pro` varchar(200) DEFAULT NULL COMMENT 'Imagen del producto',
  `nombre_pro` varchar(200) DEFAULT NULL COMMENT 'Nombre del producto',
  `descrip_pro` text COMMENT 'Descripcion del producto',
  PRIMARY KEY (`id_pro`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `productos` */

insert  into `productos`(`id_pro`,`img_pro`,`nombre_pro`,`descrip_pro`) values (1,'commerce-angle-cinema-soap.jpg','Producto 1','asdfadsf'),(2,'q25.jpg','producto 2','Descripcion ');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `username` varchar(100) NOT NULL COMMENT 'Username',
  `password` varchar(32) NOT NULL COMMENT 'Password',
  `email` varbinary(500) NOT NULL COMMENT 'Email del usaurio',
  `nombres` varchar(100) NOT NULL COMMENT 'Nombres del usaurio',
  `apellidos` varchar(100) NOT NULL COMMENT 'Apeliidos del usuario',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`username`,`password`,`email`,`nombres`,`apellidos`) values ('admin','e10adc3949ba59abbe56e057f20f883e','naycoolgonzalez@gmail.com','Naycool','Gonzalez');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
