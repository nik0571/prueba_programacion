### LEEME - README ###

	Prueba imagenamos

	En la carpeta con nombre DB que es encuesta el la raiz del sitio
	esta el archivo de la base de datos el cual podra ser importado
	desde cualquier herramient de administacion de base de datos
	como Sqlyog o phpAdmin.

### INSTALACION - INSTALLATION  ###

	Español:

		1. Copie todo el contenido del repositorio en el servidor

		2. Edite el archivo config.ini.php que se encuentra en la carpeta raíz
			- Busque la linea -> define('BASEURL','');
			- Agrege el dominio o subdomio del sitio
				Ejemplo : Define('BASEURL','http://misitio.com/');

		3. Edite todos los archivos .html  y  header.php en las etiquetas <base>
			agrege su dominio o ruta principal.
			Ejemplo: <base href="http://misitio.com/">

		Nota: La etiqueta <base> es opcional pero tenga en cuenta que si no
				se utiliza esta debera ser eliminada


### TECHNOLOGY ###

    * HTML5
    * CSS3
    * jQuery Version 1.11.2
    * jQuery UI Version 1.11.4
    * PHP Version 5.6.12
    * MySQL Version 5.6.26


### PLUGINS ###

	* html5shiv

		Español:
		Descripción: Es una librería JavaScript que permite utilizar las nuevas etiquetas o elementos
		de HTML5 en versiones de Internet Explorer anteriores a la versión 9.

		English:
		Description: A JavaScript library that allows you to use the new labels or elements
		HTML5 in previous versions of Internet Explorer to version 9 .

		- [Github:](https://github.com/afarkas/html5shiv)

	* parsley
	    - parsleyjs

	    Español:
	    Descripción: Plugin para la validacion de formularios.

	    English:
	    Plugin for form validation.

	    -[Link](http://parsleyjs.org/)
	    -[Documentation](http://parsleyjs.org/doc/index.html)


### Autor ###

* Naycool D. Gonzalez Cantillo <naycoolgonzalez@gmail.com>
* Developer Web
* Cel: 3114081801