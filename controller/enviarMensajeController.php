<?php
	/**
	 * Controlador para enviar correo de contacto
	 */
	require_once '../config.ini.php';
	require_once BASEPATH.'lib/inputfilter.php'; //libretia para prevenir ataques XSS
	require_once BASEPATH.'lib/Helpers.php'; //ayudante
	require_once BASEPATH.'lib/PHPMailer-master/PHPMailerAutoload.php'; //libreria para enviar mensajes por correo electronico

	/**
	 * [$result -> array para lamacenar el resultado
	 * 				que arrojara el controlador
	 * @var array
	 */
 	$result = array();

 	/**
 	 * Valido que llegae la variable accion y que esta contenga el valor Enviar
 	 */
	if (isset($_POST['accion']) && $_POST['accion'] == 'Enviar') {

		/**
		 * Valido que los datos introducidos en el formulario llegen y que no esten vacios
		 */
	    if(!Helpers::validDatas(array($_POST['nombres'], $_POST['email'], $_POST['asunto'], $_POST['mensaje']))){
	        $result['suceso']= false;
			$result['msj'] = 'Error: No llegaron algunos de los datos requeridos';
	    }else{

	    	/**
	    	 * [$_clean varible para llamar las funcioens de inputfilter.php
	    	 * @var InputFilter
	    	 */
			$_clean  = new InputFilter();

			/**
			 * Limpio las variables y prevengo ataque XSS
			 */
			$nombres = $_clean->process($_POST['nombres']);
			$email   = $_clean->process($_POST['email']);
			$asunto  = $_clean->process($_POST['asunto']);
			$mensaje = $_clean->process($_POST['mensaje']);

			/**
			 * Estructuro el mensaje a envuar
			 * @var PHPMailer
			 */
			$mail = new PHPMailer;
			$to = $email;
			$nombres = htmlentities($nombres);
			$email = htmlentities($email);
			$asunto = htmlentities($asunto);
			$mensaje = nl2br(htmlentities($mensaje));

			$mail->From = $email;
			$mail->FromName = $nombres;
			$mail->addAddress($to);
			$mail->Subject = $asunto;
			$mail->isHtml(true);
			$mail->Body = 'Un usuario se ha contactado desde la web <br><br> Datos del usuario <br><br> Nombres : <strong>'.$nombres.'</strong><br> Email : '.$email.'<br><br><br> Mensaje: <br><br> '.$mensaje.'';
			$mail->CharSet = 'UTF-8';

			/**
			 * Valido si se envio el mesaje o no
			 */
			if(!$mail->send()) {
				$result['suceso']= false;
				$result['msj'] = 'Error al enviar el mensaje';
			} else {
				$result['suceso']= false;
				$result['msj'] = 'Mensaje enviado!!!';
			}
	    }

	}else{
		$result['suceso']= false;
		$result['msj'] = 'No llego la variable accion';
	}

	/**
	 * Retorno una respuesta
	 */
	echo json_encode($result);

	exit;



?>