<?php
	/**
	 * Controlador para cargar el banner administrable
	 */
	require_once '../config.ini.php';
	require_once BASEPATH.'models/publicDao.php'; //Dao de la zona publica

	/**
	 * [$result -> array para lamacenar el resultado
	 * 				que arrojara el controlador
	 * @var array
	 */
 	$result = array();

 	/**
 	 * Valido que llegae la variable accion
 	 */
	if (isset($_POST['accion']) && $_POST['accion'] == 'Cargar') {

		$_obj = new publicDao();
		$banner = $_obj->cargar_banner();

		if($banner['suceso']):
			$result['suceso'] = true;
			$result['msj']= '<img src="img/banners/'.$banner['banner']->img.'" height="753" width="2000" alt="Imagen Banner">';
		else:
			$result['suceso'] = false;
			$result['msj']=$banner['suceso'];
		endif;

	}

/**
 * Retorno una respuesta
 */
echo json_encode($result);

exit;



?>