<?php
	/**
	 * Controlador para cargar productos
	 */
	require_once '../config.ini.php';
	require_once BASEPATH.'models/publicDao.php'; //Dao de la zona publica

	/**
	 * [$result -> array para lamacenar el resultado
	 * 				que arrojara el controlador
	 * @var array
	 */
 	$result = array();

 	/**
 	 * Valido que llegae la variable accion
 	 */
	if (isset($_POST['accion']) && $_POST['accion'] == 'Cargar') {

		$_obj = new publicDao();
		$productos = $_obj->load_all_productos();

		if(count($productos)>0):
			$result['suceso'] = true;
			$result['msj']= 'Productos listados';
			$result['datos']= $productos;
		else:
			$result['suceso'] = false;
			$result['msj']='No hay productos';
		endif;

	}

/**
 * Retorno una respuesta
 */
echo json_encode($result);

exit;



?>