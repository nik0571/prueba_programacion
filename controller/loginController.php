<?php
	/**
	 * Controlador de Login
	 */
	require_once '../config.ini.php';
	require_once BASEPATH.'lib/inputfilter.php'; //libretia para prevenir ataques XSS
	require_once BASEPATH.'lib/Helpers.php'; //ayudante
	require_once BASEPATH.'models/publicDao.php'; //Dao de la zona publica
	require_once BASEPATH.'lib/Simple_sessions.php'; //libreria para el control del sesiones

	/**
	 * [$result -> array para lamacenar el resultado
	 * 				que arrojara el controlador
	 * @var array
	 */
 	$result = array();

 	/**
 	 * Valido que llegae la variable accion y que esta contenga el valor Enviar
 	 */
	if (isset($_POST['accion']) && $_POST['accion'] == 'Login') {

		/**
		 * Valido que los datos introducidos en el formulario llegen y que no esten vacios
		 */
	    if(!Helpers::validDatas(array($_POST['username'], $_POST['password']))){
	        $result['suceso']= false;
			$result['msj'] = 'Error: No llegaron algunos de los datos requeridos';
	    }else{

	    	/**
	    	 * [$_clean varible para llamar las funcioens de inputfilter.php
	    	 * @var InputFilter
	    	 */
			$_clean = new InputFilter();


			$_obj   = new publicDao();

			/**
			 * Limpio las variables y prevengo ataque XSS
			 */
			$username = $_clean->process($_POST['username']);
			$password   = $_clean->process($_POST['password']);

			/**
			 * [$login Funcion para comprobar que el usuario existe
			 * y qie las credenciales son correctas
			 * @param [$username]
			 * @param [$password]
			 */
			$login = $_obj->login($username,$password);

			if($login['suceso']):
				$result['suceso'] = true;
				$result['msj'] = $login['msj'];
				$result['datos'] = $login['datos'];

				/**
				 * Utilizo la libreria simple_sesscion
				 * para el control de secciones
				 * @var Simple_sessions
				 */
				$_sesion = new Simple_sessions();
	            $data = array(
	                'username' => $login['datos']->username,
	                'email' => $login['datos']->email,
	                'nombres' => $login['datos']->nombres,
	                'apellidos' => $login['datos']->apellidos
	            );

	            $_sesion->add_sess($data);

			else:
				$result['suceso'] = false;
				$result['msj'] = $login['msj'];
			endif;
	    }

	}else{
		$result['suceso']= false;
		$result['msj'] = 'No llego la variable accion';
	}

	/**
	 * Retorno una respuesta
	 */
	echo json_encode($result);

	exit;



?>