<?php
/*
 * @Descripcion: Config.ini.php
 *  Contiene constantes globales que se requieren en la
 *  mayoría de las operaciones de base de datos o archivo
 * @author: Naycool Gonzalez C. <naycoolgonzalez@gmail.com>
 * 			Developer Web
 */

	/**
	 * Tipo de codificación
	 */
	header('Content-Type:text/html; charset=UTF-8');

	/**
	 * Definicio de constantes para el manejo de url absolutas
	 */
    define('BASEURL','http://prueba.lc/');
    define('BASEPATH',dirname(__FILE__).'/');

	/**
	 * Declaro la zona horaria
	 */
	date_default_timezone_set('America/Bogota');

?>