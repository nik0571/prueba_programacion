<?php
/*
 * @Descripcion: of helpers
 * Es una clase que contienen metodos que sirven para todas las paginas, son metodos
 * colaboradores para obtener diferentes actividades comunes y repetitivas.
 */
class Helpers {


    /*
     * Metodo pageName, obtiene el titulo de la pagina de la pagina actual
     */

    public static function pageName() {
        return substr($_SERVER["SCRIPT_NAME"], strrpos($_SERVER["SCRIPT_NAME"], "/") + 1);
    }

    /*
     * validDatas: valida si los datos post, get, request, verificando si
     *             estan vacios
    */

    public static function validDatas($datas) {
        $vacio = "";

        foreach ($datas as $d) {

            if ($d == $vacio) {
                return false;
            }
        }
        return true;
    }


    /**
     * [getRealIPBasic : Obtener la ip]
     * @return [string]
     */
    public static function getRealIPBasic(){

        if (!empty($_SERVER['HTTP_CLIENT_IP']))
                return $_SERVER['HTTP_CLIENT_IP'];

        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
                return $_SERVER['HTTP_X_FORWARDED_FOR'];

        return $_SERVER['REMOTE_ADDR'];
    }

    /**
     * [getRealIpAdvanced : obtener la Ip
     * @return [string]
     */
    public static function getRealIpAdvanced(){

        if (isset($_SERVER["HTTP_CLIENT_IP"]))
        {
            return $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
        {
            return $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
        {
            return $_SERVER["HTTP_X_FORWARDED"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
        {
            return $_SERVER["HTTP_FORWARDED_FOR"];
        }
        elseif (isset($_SERVER["HTTP_FORWARDED"]))
        {
            return $_SERVER["HTTP_FORWARDED"];
        }
        else
        {
            return $_SERVER["REMOTE_ADDR"];
        }

    }

    /**
     * [urls_amigables: funcion para limpiar caracteres y
     * asentuaciones]
     * @param  [string] $url [description]
     * @return [string]      [description]
     */
    public static function urls_amigables($url) {
           // Tranformamos todo a minusculas
           $url = strtolower($url);
           //Rememplazamos caracteres especiales latinos
           $find = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
           $repl = array('a', 'e', 'i', 'o', 'u', 'n');
           $url = str_replace ($find, $repl, $url);
           // Añadimos los guiones
           $find = array(' ', '&', '\r\n', '\n', '+');
           $url = str_replace ($find, '-', $url);
           // Eliminamos y Reemplazamos otros carácteres especiales
           $find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
           $repl = array('', '-', '');
           $url = preg_replace ($find, $repl, $url);
           return $url;
    }


}
